#define _XOPEN_SOURCE
#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <time.h>

#include "database.h"

void report(int argc, char** argv) {
    char buf[128];    
    if (argc == 1 || argc == 2) {
        time_t t0, t1;
        struct tm x;
        memset(&x, 0, sizeof(struct tm));
        strptime(argv[0], "%Y-%m-%d", &x);
        t0 = mktime(&x);
        if (argc == 2) {
            memset(&x, 0, sizeof(struct tm));
            strptime(argv[1], "%Y-%m-%d", &x);
            t1 = mktime(&x);        
        } else {
            t1 = t0 + 86400;
        }
        for (int j=t0; j<t1; j+=86400) {
            time_t u0 = j;
            time_t u1 = j + 86400;
            strftime(buf, 128, "%Y-%m-%d", localtime(&u0));
            printf("%s\n", buf);
            reportForTimespan(u0, u1);
        }
    } else {
        printf("USAGE: report <start date> [end date]\n");
    }
}

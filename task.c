#include <stdio.h>
#include <string.h>
#include <time.h>
#include "database.h"

void task(int argc, char** argv) {
    if (argc == 0) {
        listTasks();
    } else if (argc < 2) {
        printf("USAGE: task <add|del> [task [description]]\n");
    } else if (!strcmp(argv[0], "add")) {
        if (argc < 3) {
            printf("USAGE: task add <task> <description>\n");
        } else {
            if (createTask(argv[1], argv[2])) {
                printf("Task [%s] added\n", argv[1]);
            } else {
                printf("Unable to add task [%s]\n", argv[1]);
            }
        }
    } else if (!strcmp(argv[0], "del")) {
        if (deleteTask(argv[1])) {
            printf("Task [%s] deleted\n", argv[1]);
        } else {
            printf("Unable to delete task [%s]\n", argv[1]);
        }
    } else {
        printf("Invalid Command: '%s'\nUSAGE: task <add|del> [task [description]]\n", argv[0]);
    }
}

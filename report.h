#ifndef REPORT_H
#define REPORT_H

#ifdef __cplusplus
extern "C" {
#endif

void report(int, char**);

#ifdef __cplusplus
}
#endif

#endif /* REPORT_H */


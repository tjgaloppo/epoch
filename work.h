/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

/* 
 * File:   work.h
 * Author: tgaloppo
 *
 * Created on November 14, 2016, 9:29 AM
 */

#ifndef WORK_H
#define WORK_H

#ifdef __cplusplus
extern "C" {
#endif

void start(int, char**);
void stop(int, char**);
void status(int, char**);
    
#ifdef __cplusplus
}
#endif

#endif /* WORK_H */


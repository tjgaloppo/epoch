#ifndef DATABASE_H
#define DATABASE_H

#ifdef __cplusplus
extern "C" {
#endif

void dbCheck();

void listTasks();
void listCurrentTask();

int createTask(char*, char*);
int deleteTask(char*);

int endRunningTask();
int beginTask(char*);

void reportForTimespan(time_t, time_t);

#ifdef __cplusplus
}
#endif

#endif /* DATABASE_H */


#include <stdio.h>
#include <time.h>
#include "database.h"

void start(int argc, char** argv) {
    if (argc == 1) {
        endRunningTask();
        if (beginTask(argv[0])) {
            printf("Current task set to [%s]\n", argv[0]);
        }
    } else {
        printf("USAGE: start <task>\n");
    }
}

void stop(int argc, char** argv) {
    endRunningTask();
}

void status(int argc, char** argv) {
    listCurrentTask();
}

#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <unistd.h>
#include <time.h>
#include <sys/stat.h>
#include <pwd.h>
#include <sqlite3.h>

void computeTime(long delta, long* hours, long* minutes, long* seconds) {
    *hours = delta / 3600;
    *minutes = (delta % 3600) / 60;
    *seconds = ((delta % 3600) % 60);    
}

char* getHomeDirectory() {
    char* home = getenv("HOME");
    if (NULL == home) {
        struct passwd* pwd = getpwuid(getuid());
        if (NULL != pwd) {
            home = pwd->pw_dir;
        }
    }
    return home;
}

void createDatabase() {
    char* home = getHomeDirectory();
    int length = strlen(home) + 32;
    char* buf = malloc(length);
    if (buf) {
        sqlite3* db = NULL;
        snprintf(buf, length-1, "%s/.epoch", home);
        mkdir(buf, S_IRUSR | S_IWUSR | S_IXUSR);
        snprintf(buf, length-1, "%s/.epoch/epoch.db", home);
        if (SQLITE_OK == sqlite3_open(buf, &db)) {
            sqlite3_exec(db, "CREATE TABLE tasks ( name TEXT PRIMARY KEY, desc TEXT )", NULL, NULL, NULL);
            sqlite3_exec(db, "CREATE TABLE work ( task TEXT, start INTEGER, stop INTEGER )", NULL, NULL, NULL);
            sqlite3_exec(db, "CREATE TABLE working ( task TEXT, start INTEGER )", NULL, NULL, NULL);
        } 
        if (NULL != db) {
            sqlite3_close(db);
        }
        free(buf);
    }
}

void dbCheck() {
    char* home = getHomeDirectory();
    int length = strlen(home) + 32;
    char* dbFile = malloc(length);
    if (dbFile) {
        snprintf(dbFile, length - 1, "%s/.epoch/epoch.db", home);
        if (access(dbFile, F_OK) == -1) {
            printf("Initializing database\n");
            createDatabase(home);
        }
        free(dbFile);
    }
}

sqlite3* open_database() {
    sqlite3* db = NULL;
    char* home = getHomeDirectory();
    int length = strlen(home) + 32;
    char* buf = malloc(length);
    if (buf) {
        snprintf(buf, length-1, "%s/.epoch/epoch.db", home);
        if (SQLITE_OK != sqlite3_open(buf, &db)) {
            sqlite3_close(db);
            db = NULL;
        }
        free(buf);
    }
    return db;
}

int printTask(void* unused, int ncol, char** vals, char** names) {
    printf("%15s | %s\n", vals[0], vals[1]);
    return 0;
}

void listTasks() {
    sqlite3* db = open_database();
    if (NULL != db) {
        sqlite3_exec(db, "SELECT name, desc FROM tasks", printTask, NULL, NULL);
        sqlite3_close(db);
    }
}

int createTask(char *name, char* desc) {
    int returnVal = 0;
    char sql[1024];
    snprintf(sql, 1023, "INSERT INTO tasks VALUES ('%s', '%s')", name, desc);
    sqlite3* db = open_database();
    if (NULL != db) {
        if (SQLITE_OK == sqlite3_exec(db, sql, NULL, NULL, NULL)) {
            returnVal = 1;
        }
        sqlite3_close(db);
    }    
    return returnVal;
}

int deleteTask(char* name) {
    int returnVal = 0;
    char sql[1024];
    snprintf(sql, 1023, "DELETE FROM tasks WHERE name='%s'", name);
    sqlite3* db = open_database();
    if (NULL != db) {
        if (SQLITE_OK == sqlite3_exec(db, sql, NULL, NULL, NULL)) {
            returnVal = 1;
        }
        sqlite3_close(db);
    }        
    return returnVal;
}

int endRunningTask() {
    sqlite3* db = open_database();
    if (NULL != db) {
        sqlite3_stmt* statement;
        sqlite3_prepare(db, "SELECT task, start FROM working", -1, &statement, NULL);
        if (SQLITE_ROW == sqlite3_step(statement)) {
            time_t now = time(NULL);
            const char* name = sqlite3_column_text(statement, 0);
            long  t0   = sqlite3_column_int64(statement, 1);
            long delta = now - t0;
            if (delta > 8 * 3600) { // do not allow > 8 hour interval
                now = t0 + 8 * 3600;
                delta = now - t0;
            }
            long hours, minutes, seconds;
            computeTime(delta, &hours, &minutes, &seconds);
            printf("Ending task [%s]; time = %02ld:%02ld:%02ld\n", name, hours, minutes, seconds);
            char sql[1024];
            snprintf(sql, 1023, "INSERT INTO work VALUES ('%s', %ld, %ld)", name, t0, now);
            if (SQLITE_OK != sqlite3_exec(db, sql, NULL, NULL, NULL)) {
                printf("** ERROR RECORDING WORK LOG **\n");
            }
        } else {
            printf("No current working task is set\n");
        }
        sqlite3_finalize(statement);
        sqlite3_exec(db, "DELETE FROM working", NULL, NULL, NULL);
        sqlite3_close(db);
    }
    return 0;
}

int beginTask(char* name) {
    int returnVal = 0;
    sqlite3* db = open_database();
    if (NULL != db) {
        char sql[1024];
        snprintf(sql, 1023, "SELECT name FROM tasks WHERE name='%s'", name);
        sqlite3_stmt* statement;
        sqlite3_prepare(db, sql, -1, &statement, NULL);
        if (SQLITE_ROW == sqlite3_step(statement)) {
            char sql2[1024];
            snprintf(sql2, 1023, "INSERT INTO working VALUES ('%s', %ld)", name, time(NULL));
            if (SQLITE_OK != sqlite3_exec(db, sql2, NULL, NULL, NULL)) {
                printf("** ERROR STARTING WORK LOG **\n");
            } else {
                returnVal = 1;
            }
        } else {
            printf("Task [%s] does not exist\n", name);
        }
        sqlite3_finalize(statement);
        sqlite3_close(db);
    }
    return returnVal;
}

void listCurrentTask() {
    sqlite3* db = open_database();
    if (NULL != db) {
        time_t now = time(NULL);
        sqlite3_stmt* statement;
        sqlite3_prepare(db, "SELECT task, start FROM working", -1, &statement, NULL);
        if (SQLITE_ROW == sqlite3_step(statement)) {
            const char* name = sqlite3_column_text(statement, 0);
            long t0 = sqlite3_column_int64(statement, 1);
            long delta = now - t0, hours, minutes, seconds;
            computeTime(delta, &hours, &minutes, &seconds);
            printf("Current task is [%s], time = %02ld:%02ld:%02ld\n", name, hours, minutes, seconds);
        } else {
            printf("No current working task is set\n");
        }
        sqlite3_finalize(statement);
        sqlite3_close(db);
    }
}

void reportForTimespan(time_t tStart, time_t tEnd) {
    sqlite3* db = open_database();
    if (NULL != db) {
        char sql[1024];
        snprintf(sql, 1023, "SELECT task, sum(stop-start) FROM work WHERE (start > %d AND start < %d) OR (stop > %d AND stop < %d) GROUP BY task", tStart, tEnd, tStart, tEnd);
        sqlite3_stmt* statement;
        sqlite3_prepare(db, sql, -1, &statement, NULL);
        while (SQLITE_ROW == sqlite3_step(statement)) {
            const char* name = sqlite3_column_text(statement, 0);
            long total = sqlite3_column_int64(statement, 1);
            long hours, minutes, seconds;
            computeTime(total, &hours, &minutes, &seconds);
            printf("  %15s : %02ld:%02ld:%02ld\n", name, hours, minutes, seconds);
        }
        sqlite3_close(db);
    }
}

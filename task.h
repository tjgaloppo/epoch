#ifndef TASK_H
#define TASK_H

#ifdef __cplusplus
extern "C" {
#endif

void task(int, char**);


#ifdef __cplusplus
}
#endif

#endif /* TASK_H */


#include <stdio.h>
#include <string.h>
#include <stdlib.h>
#include <unistd.h>

#include "database.h"
#include "task.h"
#include "work.h"
#include "report.h"

typedef struct {
    char* name;
    void (*func)(int, char**);
} cmd_t, *pcmd;

void printUsage(char* exeName) {
    printf("USAGE: %s <command> [parameters]+\n", exeName);
}

cmd_t commands[] = {
    { "report" , report },
    { "start"  , start },
    { "status" , status },
    { "stop"   , stop },
    { "task"   , task },
};
int ncmd = sizeof(commands) / sizeof(commands[0]);

int cmpCommandName(const void* arg1, const void* arg2) {
    const char* key = (char*)arg1;
    const pcmd  cmd = (pcmd)arg2;
    return strcmp(key, cmd->name);
}

int main(int argc, char** argv) {
    dbCheck();
    
    if (argc < 2) {
        printUsage(argv[0]);
    } else {
        pcmd cmd = bsearch(argv[1], commands, ncmd, sizeof(cmd_t), cmpCommandName);
        if (NULL != cmd) {
            cmd->func(argc-2, argv+2);
        } else {
            printUsage(argv[0]);
        }
    }
    return (EXIT_SUCCESS);
}
